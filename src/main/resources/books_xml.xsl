<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
    <xsl:import-schema schema-location="library.xsd" />
    <xsl:output method="xml" encoding="utf-8"/>

    <xsl:template match="/">
        <books>
            <xsl:for-each select="//book">
                <book>
                    <id><xsl:value-of select="@id"/></id>
                    <title><xsl:value-of select="title"/></title>
                </book>
            </xsl:for-each>
        </books>
    </xsl:template>

</xsl:stylesheet>