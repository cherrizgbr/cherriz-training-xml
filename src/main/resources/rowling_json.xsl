<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
    <xsl:output method="text" encoding="UTF-8" media-type="application/json"/>

    <xsl:template match="/">
        {
            "beschreibung": "Alle Bücher von Joanne K. Rowling",
            "buecher": [
                <xsl:for-each select="//book">
                    <xsl:if test="author = 'Joanne K. Rowling'">
                        <xsl:if test="position() > 1">,</xsl:if>
                        {
                            "title": "<xsl:value-of select="title"/>",
                            "author": "<xsl:value-of select="author"/>",
                            "released": <xsl:value-of select="released"/>
                        }
                    </xsl:if>
                </xsl:for-each>
            ]
        }
    </xsl:template>

</xsl:stylesheet>