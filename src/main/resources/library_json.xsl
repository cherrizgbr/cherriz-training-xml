<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs">
    <xsl:import-schema schema-location="library.xsd" />
    <xsl:output method="text" encoding="UTF-8" media-type="text/plain"/>

    <xsl:template match="/">
        {
        "name": "<xsl:value-of select="//name"/>",
        "location": "<xsl:value-of select="//location"/>",
        "url": "<xsl:value-of select="//url"/>",
        "books": { 'book': [

        <xsl:for-each select="//book">{
            "id": <xsl:value-of select="@id"/>,
            "title": "<xsl:value-of select="title"/>",
            "author": "<xsl:value-of select="author"/>",
            "released":
            <xsl:value-of select="released"/>}
            <xsl:if test="position() != last()">,</xsl:if>
        </xsl:for-each>
        <xsl:text>]}}</xsl:text>

    </xsl:template>

</xsl:stylesheet>