package de.cherriz.training.xml.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class DOMWorker {

    public static void main(String[] args) throws Exception {
        DOMWorker domWorker = new DOMWorker();

        Document document = domWorker.readFile("library.xml");

        domWorker.removeBook(document, "1984");

        domWorker.addBook(document, "Animal Farm", "George Orwell", 1945);

        System.out.println(domWorker.traversXML(document.getChildNodes()));
    }

    public Document readFile(String fileName) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(fileName);
        return builder.parse(resource);
    }

    public String traversXML(NodeList nodes) {
        String result = "";
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                continue;
            }
            result += "\n" + node.getNodeName();
            if (node.getChildNodes().getLength() == 1 && node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE) {
                result += ": " + node.getChildNodes().item(0).getNodeValue();
            } else {
                result += traversXML(node.getChildNodes()).replace("\n", "\n\t");
            }
        }
        return result;
    }

    public void removeBook(Document doc, String title) {
        NodeList books = doc.getElementsByTagName("book");
        for (int i = 0; i < books.getLength(); i++) {
            Node node = books.item(i);
            if(node.getChildNodes().item(1).getChildNodes().item(0).getNodeValue().equals(title)){
                node.getParentNode().removeChild(node);
                return;
            }
        }
    }

    public void addBook(Document doc, String title, String author, int released){
        Node books = doc.getElementsByTagName("books").item(0);

        Element book = doc.createElement("book");
        book.appendChild(doc.createElement("title"));
        book.appendChild(doc.createElement("author"));
        book.appendChild(doc.createElement("released"));
        book.getChildNodes().item(0).appendChild(doc.createTextNode(title));
        book.getChildNodes().item(1).appendChild(doc.createTextNode(author));
        book.getChildNodes().item(2).appendChild(doc.createTextNode(Integer.valueOf(released).toString()));

        books.appendChild(book);
    }

}