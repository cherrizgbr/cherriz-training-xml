package de.cherriz.training.xml.xslt;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringWriter;

public class XSLTransformer {

    public static void main(String args[]) throws TransformerException {
        XSLTransformer transformer = new XSLTransformer();

        Source xml = transformer.readFile("library.xml");
        Source xslt = transformer.readFile("library_json.xsl");

        String result = transformer.transform(xml, xslt);

        System.out.println(result);
    }

    public Source readFile(String name) {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(name);
        return new StreamSource(resource);
    }

    public String transform(Source xml, Source xslt) throws TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(xslt);

        StringWriter outWriter = new StringWriter();
        StreamResult result = new StreamResult(outWriter);
        transformer.transform(xml, result);

        return outWriter.getBuffer().toString();
    }

}