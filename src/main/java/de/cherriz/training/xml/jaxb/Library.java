package de.cherriz.training.xml.jaxb;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Library {

    public String name;

    public String location;

    public String url;

    public Books books;

    @Override
    public String toString() {
        return name + " in " + location + "\n" + url + "\nBücher:\n" + books;
    }

}
