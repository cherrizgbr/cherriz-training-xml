package de.cherriz.training.xml.jaxb;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Books {

    public List<Book> book;

    public void addBook(Book item) {
        book.add(item);
    }

    public Book getBook(String title) {
        for (Book item : book) {
            if (item.title.equals(title)) {
                return item;
            }
        }
        return null;
    }

    public void removeBook(Book item) {
        book.remove(item);
    }

    @Override
    public String toString() {
        String result = "";
        for (Book item : book) {
            result += item;
        }
        return result;
    }

}