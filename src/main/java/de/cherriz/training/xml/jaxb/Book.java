package de.cherriz.training.xml.jaxb;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Book {

    public String title;

    public String author;

    public int released;

    public Book() {}

    public Book(String title, String author, int released) {
        this.title = title;
        this.author = author;
        this.released = released;
    }

    @Override
    public String toString() {
        return "\t" + title + "\n\tVeröffentlicht am " + released + " von " + author + "\n";
    }

}