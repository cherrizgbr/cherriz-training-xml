package de.cherriz.training.xml.jaxb;

import javax.xml.bind.JAXB;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class JAXBWorker {

    public static void main(String args[]) {
        JAXBWorker jaxbWorker = new JAXBWorker();

        Library library = jaxbWorker.readXML("library.xml");
        System.out.println(library);

        Book book = library.books.getBook("1984");
        library.books.removeBook(book);
        library.books.addBook(new Book("Animal Farm", "George Orwell", 1945));

        String xml = jaxbWorker.createXML(library);
        System.out.println(xml);
    }

    public Library readXML(String fileName) {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(fileName);
        return JAXB.unmarshal(resource, Library.class);
    }

    public String createXML(Library library) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        JAXB.marshal(library, stream);
        return stream.toString();
    }

}