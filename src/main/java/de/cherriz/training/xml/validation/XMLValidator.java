package de.cherriz.training.xml.validation;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;

public class XMLValidator {

    public static void main(String args[]) {
        XMLValidator validator = new XMLValidator();

        InputStream xml = validator.readFile("library.xml");
        InputStream xsd = validator.readFile("library.xsd");

        String error = validator.validateAgainstXSD(xml, xsd);

        if (error == null) {
            System.out.println("XML ist valide!");
        } else {
            System.out.println(error);
        }
    }

    public String validateAgainstXSD(InputStream xml, InputStream xsd) {
        Validator validator = createValidator(xsd);

        if (validator == null) {
            return "Validator konnte nicht erzeugt werden.";
        }

        try {
            validator.validate(new StreamSource(xml));
            return null;
        } catch (SAXException e) {
            return "XML-Datei nicht valide:\n" + e.getMessage();
        } catch (IOException e) {
            return "XML-Datei konnte nicht gelesen werden";
        }
    }

    private Validator createValidator(InputStream xsd) {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));
            return schema.newValidator();
        } catch (SAXException e) {
            System.out.println("Fehler beim Erzeugen des Validarots:\n" + e.getMessage());
        }
        return null;
    }

    public InputStream readFile(String fileName) {
        return this.getClass().getClassLoader().getResourceAsStream(fileName);
    }

}