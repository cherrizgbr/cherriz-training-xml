package de.cherriz.training.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXSearch extends DefaultHandler {

    String search;

    String result;

    int layer;

    SAXSearch(String search) {
        this.search = search;
    }

    public String getResult() {
        return result;
    }

    @Override
    public void startDocument() throws SAXException {
        result = "";
        layer = -1;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String str = "";
        for (int i = start; i < length + start; i++) {
            str += ch[i];
        }
        if (str.trim().length() > 0 && layer >= 0) {
            result = result.substring(0, result.length() - 1) + ": " + str + "\n";
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (search.equals(qName)) {
            layer = 0;
            result += qName + "\n";
        } else if (layer >= 0) {
            layer++;
            result += qName + "\n";
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (layer >= 0) {
            layer--;
        }
    }

}