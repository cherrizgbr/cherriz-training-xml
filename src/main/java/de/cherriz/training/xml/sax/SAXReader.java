package de.cherriz.training.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXReader extends DefaultHandler {

    String content;

    String spacer;

    public String getContent() {
        return content;
    }

    @Override
    public void startDocument() throws SAXException {
        content = "";
        spacer = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String str = "";
        for (int i = start; i < length + start; i++) {
            str += ch[i];
        }
        if (str.trim().length() > 0) {
            content = content.substring(0, content.length() - 1) + ": " + str + "\n";
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        spacer += "\t";
        content += spacer + qName + "\n";
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        spacer = spacer.substring(0, spacer.length() - 1);
    }

}