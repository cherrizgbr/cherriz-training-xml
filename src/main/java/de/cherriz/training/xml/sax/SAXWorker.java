package de.cherriz.training.xml.sax;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

public class SAXWorker extends DefaultHandler {

    public static void main(String args[]) throws IOException, SAXException, ParserConfigurationException {
        SAXWorker saxWorker = new SAXWorker();

        SAXReader saxReader = saxWorker.readFile("library.xml", new SAXReader());

        System.out.println(saxReader.getContent());

        SAXSearch saxSearch = saxWorker.readFile("library.xml", new SAXSearch("url"));

        System.out.println(saxSearch.getResult());
    }

    public <T extends DefaultHandler> T readFile(String fileName, T handler) throws ParserConfigurationException, SAXException, IOException {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(fileName);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(resource, handler);
        return handler;
    }

}