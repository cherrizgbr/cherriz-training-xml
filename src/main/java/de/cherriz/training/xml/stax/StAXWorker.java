package de.cherriz.training.xml.stax;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;

public class StAXWorker {

    public static void main(String[] args) throws Exception {
        StAXWorker stAXWorker = new StAXWorker();

        XMLStreamReader parser = stAXWorker.readFile("library.xml");

        System.out.println(stAXWorker.traversXML(parser));

        parser = stAXWorker.readFile("library.xml");

        System.out.println(stAXWorker.search(parser, "url"));
    }

    public XMLStreamReader readFile(String fileName) throws ParserConfigurationException, IOException, SAXException, XMLStreamException {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(fileName);

        XMLInputFactory factory = XMLInputFactory.newInstance();
        return factory.createXMLStreamReader(resource);
    }

    public String traversXML(XMLStreamReader parser) throws XMLStreamException {
        String result = "";
        String spacer = "";

        while (parser.hasNext()) {
            switch (parser.getEventType()) {
                case XMLStreamConstants.END_DOCUMENT:
                    parser.close();
                    break;
                case XMLStreamConstants.START_ELEMENT:
                    spacer += "\t";
                    result += spacer + parser.getLocalName() + "\n";
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if (!parser.isWhiteSpace()) {
                        result = result.substring(0, result.length() - 1) + ": " + parser.getText() + "\n";
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    spacer = spacer.substring(0, spacer.length() - 1);
                    break;
                default:
                    break;
            }
            parser.next();
        }

        return result;
    }

    public String search(XMLStreamReader parser, String node) throws XMLStreamException {
        int layer = -1;
        String result = "";
        while (parser.hasNext()) {
            switch (parser.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    if (node.equals(parser.getLocalName())) {
                        layer = 0;
                        result += parser.getLocalName() + "\n";
                    } else if (layer >= 0) {
                        layer++;
                        result += parser.getLocalName() + "\n";
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if (!parser.isWhiteSpace() && layer >= 0) {
                        result = result.substring(0, result.length() - 1) + ": " + parser.getText() + "\n";
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (layer == 0) {
                        parser.close();
                        return result;
                    } else if (layer > 0) {
                        layer--;
                    }
                    break;
                default:
                    break;
            }
            parser.next();
        }

        parser.close();
        return null;
    }

}